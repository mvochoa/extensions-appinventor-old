package com.mvochoa.base64;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.appinventor.components.annotations.DesignerComponent;
import com.google.appinventor.components.annotations.SimpleFunction;
import com.google.appinventor.components.annotations.SimpleObject;
import com.google.appinventor.components.common.ComponentCategory;
import com.google.appinventor.components.runtime.AndroidNonvisibleComponent;
import com.google.appinventor.components.runtime.ComponentContainer;
import com.google.appinventor.components.runtime.util.ErrorMessages;
import com.google.appinventor.components.annotations.UsesPermissions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 *
 * @author mvochoa
 */
@DesignerComponent(version = 1,
        description = "Codificardor y Decodificar de archivos en base64",
        category = ComponentCategory.EXTENSION,
        nonVisible = true,
        iconName = "images/extension.png")
@SimpleObject(external = true)
@UsesPermissions(permissionNames = "android.permission.WRITE_EXTERNAL_STORAGE, android.permission.READ_EXTERNAL_STORAGE")
public class Base64 extends AndroidNonvisibleComponent {
    Context context;

    public Base64(ComponentContainer container) {
        super(container.$form());
        context = (Context)container.$context();
    }

    @SimpleFunction(description = "Retorna una cadena en base64 del archivo.")
    public String ImageToBase64(String path) {
        String base64 = "";
        try {
            Bitmap bm = BitmapFactory.decodeFile(path);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArrayImage = baos.toByteArray();

            base64 = android.util.Base64.encodeToString(byteArrayImage, android.util.Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            form.dispatchErrorOccurredEvent(this, "ioBase64",
                    ErrorMessages.ERROR_CANVAS_BITMAP_ERROR, e.getMessage());
        }
        return base64;
    }

    @SimpleFunction(description = "Retorna la ruta del archivo decodicado.")
    public String Base64ToImage(String base64) {
        String name = (System.currentTimeMillis() / 1000L) + "";
        try {
            File img = File.createTempFile(name, null, context.getExternalCacheDir());
            name = img.getAbsolutePath();

            FileOutputStream fos = new FileOutputStream(img);
            fos.write(android.util.Base64.decode(base64, android.util.Base64.DEFAULT));
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            form.dispatchErrorOccurredEvent(this, "ioBase64",
                    ErrorMessages.ERROR_CANNOT_SAVE_IMAGE, e.getMessage());
        }

        return name;
    }
}
